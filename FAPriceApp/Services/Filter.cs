﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text.RegularExpressions;
using FAPriceApp.Models;
using FAPriceApp.Support;

namespace FAPriceApp.Services
{
    public class Filter
    {
        private readonly string Basic = "Basic";
        private readonly string EaglePre = "Eagle Premier";
        private readonly string ValuePlus = "Value Plus";
        public static readonly string NotAc = "N/A";
        public static readonly string Inc = "Inc.";
        private static readonly string IncNoDot = "Inc";
        private readonly string under5k = "UNDER5000";
        private readonly string over5k = "OVER5000";
        private readonly string multiplex = "MULTIPLEX";
        
        #region Prop
        
        public List<PropertyTypes> CreateListOfPropertyTypesList(string exportTable, string stateId)
        {
            if (string.IsNullOrEmpty(stateId))
            {
                throw new Exception("No state id");
            }
            var splitTable = exportTable.Split(@"$", StringSplitOptions.RemoveEmptyEntries);
            var listOfProps = new List<PropertyTypes>();
            foreach (var entry in splitTable)
            {
                if (string.Equals(entry, Environment.NewLine, StringComparison.OrdinalIgnoreCase) || string.IsNullOrEmpty(entry) || entry.Contains(Environment.NewLine))
                {
                    continue;
                }
                
                var items = ProcessEntryFormatting(entry);
                var propId = PropertyTypes.MapProp(items[0]);
                if (string.IsNullOrEmpty(propId))
                {
                    //For Guest House prop types, still want an error be thrown if there is an unknown type.
                    continue;
                }
                var mappedPropValues = MapPropertyValues(items, propId, stateId);
                if (!mappedPropValues.Any())
                {
                    continue;
                }

                listOfProps.AddRange(mappedPropValues);
            }

            return listOfProps;
        }
        
        private string[] ProcessEntryFormatting(string entry)
        {
            var items = Regex.Split(entry, "[,]{1}(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
            var alteredItems = new List<string>();
            foreach (var item in items)
            {
                if (string.IsNullOrEmpty(item))
                {
                    continue;
                }
                if (!item.Contains('"'))
                {
                    alteredItems.Add(item);
                    continue;
                }

                alteredItems.Add(item.Replace('"', ' ').Trim());
            }
           
            return alteredItems.ToArray();
        }

        private List<PropertyTypes> MapPropertyValues(string[] items, string propId, string stateId)
        {
            var listOfProps = new List<PropertyTypes>();
            var fees = MapFeeStructure(items);
            var basicModelOneYear = new PropertyTypes
            {
                CoverageId = Basic,
                PropertyTypeId = propId,
                StateId = stateId,
                Fee = fees.BasicOne,
                YearCoverage = 1
            }.TryAddToList(listOfProps);

            var basicModelTwoYear = new PropertyTypes
            {
                CoverageId = Basic,
                PropertyTypeId = propId,
                StateId = stateId,
                Fee = fees.BasicTwo,
                YearCoverage = 2
            }.TryAddToList(listOfProps);

            var valueModelOneYear = new PropertyTypes
            {
                CoverageId = ValuePlus,
                PropertyTypeId = propId,
                StateId = stateId,
                Fee = fees.ValueOne,
                YearCoverage = 1
            }.TryAddToList(listOfProps);
            
            var valueModelTwoYear = new PropertyTypes
            {
                CoverageId = ValuePlus,
                PropertyTypeId = propId,
                StateId = stateId,
                Fee = fees.ValueTwo,
                YearCoverage = 2
            }.TryAddToList(listOfProps);

            var eaglePreOneYear = new PropertyTypes
            {
                CoverageId = EaglePre,
                PropertyTypeId = propId,
                StateId = stateId,
                Fee = fees.EaglePreOne,
                YearCoverage = 1
            }.TryAddToList(listOfProps);

            var eaglePreTwoYear = new PropertyTypes
            {
                CoverageId = EaglePre,
                PropertyTypeId = propId,
                StateId = stateId,
                Fee = fees.EaglePreTwo,
                YearCoverage = 2
            }.TryAddToList(listOfProps);
            
            return listOfProps;
        }

        private FeeStructure MapFeeStructure(string[] items)
        {
          var fees = new FeeStructure();
          fees.BasicOne = items[1];
          fees.BasicTwo = items[2];
          if (items.Length == 7)
          {
              fees.ValueOne = items[3];
              fees.ValueTwo = items[4];
              fees.EaglePreOne = items[5];
              fees.EaglePreTwo = items[6];
              return fees;
          }
          fees.EaglePreOne = items[3];
          fees.EaglePreTwo = items[4];
          return fees;
        }
        #endregion

        #region Options

        public List<OptionsTypes> CreateListOfOptionsTypesList(string exportTable, string stateId)
        {
            if (string.IsNullOrEmpty(stateId))
            {
                throw new Exception("No state id");
            }
            var splitTable = exportTable.Split(@"$", StringSplitOptions.RemoveEmptyEntries);
            var listOfProps = new List<OptionsTypes>();
            foreach (var entry in splitTable)
            {
                if (string.Equals(entry, Environment.NewLine, StringComparison.OrdinalIgnoreCase) || string.IsNullOrEmpty(entry))
                {
                    continue;
                }
                
                var items = ProcessEntryFormatting(entry);
                var fees = MapOptionFeeStructure(items);
                var basicOptUnder5kOneYear = new OptionsTypes
                {
                    OptionId = items[0],
                    PropertyTypeCategory = under5k,
                    CoverageId = Basic,
                    StateId = stateId,
                    Fee = fees.BasicOneUnder5k == Inc || fees.BasicOneUnder5k == IncNoDot ? "0" : fees.BasicOneUnder5k,
                    IsIncluded = fees.BasicOneUnder5k == Inc || fees.BasicOneUnder5k == IncNoDot,
                    IsFeeByUnit = false,
                    YearCoverage = 1
                }.TryAddToList(listOfProps);
               
                
                var basicOptOver5kOneYear = new OptionsTypes
                {
                    OptionId = items[0],
                    PropertyTypeCategory = over5k,
                    CoverageId = Basic,
                    StateId = stateId,
                    Fee = fees.BasicOneOver5k == Inc || fees.BasicOneOver5k == IncNoDot ? "0" : fees.BasicOneOver5k,
                    IsIncluded = fees.BasicOneOver5k == Inc || fees.BasicOneOver5k == IncNoDot,
                    IsFeeByUnit = false,
                    YearCoverage = 1
                }.TryAddToList(listOfProps);
                
                
                var basicOptUnder5kTwoYear = new OptionsTypes
                {
                    OptionId = items[0],
                    PropertyTypeCategory = under5k,
                    CoverageId = Basic,
                    StateId = stateId,
                    Fee = fees.BasicTwoUnder5k == Inc || fees.BasicTwoUnder5k == IncNoDot ? "0" : fees.BasicTwoUnder5k,
                    IsIncluded = fees.BasicTwoUnder5k == Inc || fees.BasicTwoUnder5k == IncNoDot,
                    IsFeeByUnit = false,
                    YearCoverage = 2
                }.TryAddToList(listOfProps);
                
                
                var basicOptOver5kTwoYear = new OptionsTypes
                {
                    OptionId = items[0],
                    PropertyTypeCategory = over5k,
                    CoverageId = Basic,
                    StateId = stateId,
                    Fee = fees.BasicTwoOver5k == Inc || fees.BasicTwoOver5k == IncNoDot ? "0" : fees.BasicTwoOver5k,
                    IsIncluded = fees.BasicTwoOver5k == Inc || fees.BasicTwoOver5k == IncNoDot,
                    IsFeeByUnit = false,
                    YearCoverage = 2
                }.TryAddToList(listOfProps);
                
                
                var basicOptMultiOneYear = new OptionsTypes
                {
                    OptionId = items[0],
                    PropertyTypeCategory = multiplex,
                    CoverageId = Basic,
                    StateId = stateId,
                    Fee = fees.BasicOneMulti == Inc || fees.BasicOneMulti == IncNoDot ? "0" : fees.BasicOneMulti,
                    IsIncluded = fees.BasicOneMulti == Inc || fees.BasicOneMulti == IncNoDot,
                    IsFeeByUnit = true,
                    YearCoverage = 1
                }.TryAddToList(listOfProps);
               
                
                var basicOptMultiTwoYear = new OptionsTypes
                {
                    OptionId = items[0],
                    PropertyTypeCategory = multiplex,
                    CoverageId = Basic,
                    StateId = stateId,
                    Fee = fees.BasicTwoMulti == Inc || fees.BasicTwoMulti == IncNoDot ? "0" : fees.BasicTwoMulti,
                    IsIncluded = fees.BasicTwoMulti == Inc || fees.BasicTwoMulti == IncNoDot,
                    IsFeeByUnit = true,
                    YearCoverage = 2
                }.TryAddToList(listOfProps);
                

                var valueOptUnder5kOneYear = new OptionsTypes
                {
                    OptionId = items[0],
                    PropertyTypeCategory = under5k,
                    CoverageId = ValuePlus,
                    StateId = stateId,
                    Fee = fees.ValueOneUnder5k == Inc || fees.ValueOneUnder5k == IncNoDot ? "0" : fees.ValueOneUnder5k,
                    IsIncluded = fees.ValueOneUnder5k == Inc || fees.ValueOneUnder5k == IncNoDot,
                    IsFeeByUnit = false,
                    YearCoverage = 1
                }.TryAddToList(listOfProps);

                var valueOptUnder5kTwoYear = new OptionsTypes
                {
                    OptionId = items[0],
                    PropertyTypeCategory = under5k,
                    CoverageId = ValuePlus,
                    StateId = stateId,
                    Fee = fees.ValueTwoUnder5k == Inc || fees.ValueTwoUnder5k == IncNoDot ? "0" : fees.ValueTwoUnder5k,
                    IsIncluded = fees.ValueTwoUnder5k == Inc || fees.ValueTwoUnder5k == IncNoDot,
                    IsFeeByUnit = false,
                    YearCoverage = 2
                }.TryAddToList(listOfProps);

                var eaglePreOptUnder5kOneYear = new OptionsTypes
                {
                    OptionId = items[0],
                    PropertyTypeCategory = under5k,
                    CoverageId = EaglePre,
                    StateId = stateId,
                    Fee = fees.EaglePreOneUnder5k == Inc || fees.EaglePreOneUnder5k == IncNoDot ? "0" : fees.EaglePreOneUnder5k,
                    IsIncluded = fees.EaglePreOneUnder5k == Inc || fees.EaglePreOneUnder5k == IncNoDot,
                    IsFeeByUnit = false,
                    YearCoverage = 1
                }.TryAddToList(listOfProps);
                
                var eaglePreOptUnder5kTwoYear = new OptionsTypes
                {
                    OptionId = items[0],
                    PropertyTypeCategory = under5k,
                    CoverageId = EaglePre,
                    StateId = stateId,
                    Fee = fees.EaglePreTwoUnder5k == Inc || fees.EaglePreTwoUnder5k == IncNoDot ? "0" : fees.EaglePreTwoUnder5k,
                    IsIncluded = fees.EaglePreTwoUnder5k == Inc || fees.EaglePreTwoUnder5k == IncNoDot,
                    IsFeeByUnit = false,
                    YearCoverage = 2
                }.TryAddToList(listOfProps);

            }

            return listOfProps;
        }

        private FeeOptionStructure MapOptionFeeStructure(string[] items)
        {
            var fees = new FeeOptionStructure
            {
                BasicOneUnder5k = items[1].Trim(),
                BasicOneOver5k = items[2].Trim(),
                BasicTwoUnder5k = items[3].Trim(),
                BasicTwoOver5k = items[4].Trim(),
                BasicOneMulti = items[5].Trim(),
                BasicTwoMulti = items[6].Trim()
            };
            if (items.Length > 9)
            {
                fees.ValueOneUnder5k = items[7].Trim();
                fees.ValueTwoUnder5k = items[8].Trim();
                fees.EaglePreOneUnder5k = items[9].Trim();
                fees.EaglePreTwoUnder5k = items[10].Trim();
                return fees;
            }
            
            fees.EaglePreOneUnder5k = items[7].Trim();
            fees.EaglePreTwoUnder5k = items[8].Trim();
            return fees;
        }

        #endregion

    }
}