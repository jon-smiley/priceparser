﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FAPriceApp.Models;
using FAPriceApp.Services;
using FAPriceApp.Support;

namespace FAPriceApp
{
    class Program
    {
        //args will have Command name,
        //file to look at
        //and stateId
        //EX: Prop TN AZ CO
        static async Task Main(string[] args)
        {
            Console.WriteLine("FA Price app start.");
            Console.WriteLine("======================================================");
            Console.WriteLine("What is the folder you want to reference?");
            Global.ReferenceFile = Console.ReadLine();
            Console.WriteLine("======================================================");
            Console.WriteLine("What database are you referencing. Example [TransactionDesk].[dbo]");
            Global.Database = Console.ReadLine();
            Console.WriteLine("=======================================================");
            Console.WriteLine("What command are you preforming.");
            var tokenSource2 = new CancellationTokenSource();
            var ct = tokenSource2.Token;
            var task = Task.Run(() =>
            {
                do
                {
                    SystemCommands.ListCommands();
                    Console.WriteLine("Select a Command OR Type Exit");
                    var completeCommandLine = Console.ReadLine().ToUpper();
                    if (completeCommandLine == "EXIT")
                    {
                        tokenSource2.Cancel();
                        Console.WriteLine("Exit application.");
                    }
                    else
                    {
                        var subComands = string.Empty;
                        Console.WriteLine($"Command operation {completeCommandLine}");
                        Console.WriteLine("Add states associated with command. Or Exit.");
                        subComands = Console.ReadLine().ToUpper();
                        if (subComands == "EXIT")
                        {
                            tokenSource2.Cancel();
                            Console.WriteLine("Exit application.");
                        }
                        else
                        {
                            completeCommandLine = $"{completeCommandLine} {subComands}";
                            Run(completeCommandLine.Split(' ', StringSplitOptions.RemoveEmptyEntries));
                        }
                    }

                } while (!ct.IsCancellationRequested);
                
            }, tokenSource2.Token);
            
            
            try
            {
                await task;
            }
            catch (OperationCanceledException e)
            {
                Console.WriteLine($"{nameof(OperationCanceledException)} thrown with message: {e.Message}");
            }
            finally
            {
                tokenSource2.Dispose();
            }
        }
        public static void Run(string[] args)
        {
            //Property Types
            Console.WriteLine($"Starting Script build for {args[0]}");
            var command = args[0].ToUpper();
            var states = new List<string>(args);
            var filter = new Filter();
            var textFile = new Sql();
            
            states.RemoveAt(0);
            if (command == nameof(SystemCommands.Commands.PROP))
            {
                foreach (var state in states)
                {
                    var properties = filter.CreateListOfPropertyTypesList(textFile.GetFileInfo(command, state.ToUpper()), state.ToUpper());
                    if (properties.Any())
                    {
                        textFile.BuildPropSqlScript(properties, state.ToUpper());
                    }   
                }
            }
            else if (command == nameof(SystemCommands.Commands.SELLER))
            {
                foreach (var state in states)
                {
                    var sellerCov = filter.CreateListOfOptionsTypesList(textFile.GetFileInfo(command, state.ToUpper()), state.ToUpper());
                    if (sellerCov.Any())
                    {
                        textFile.BuildOptionsSqlScript(sellerCov, state.ToUpper(), true);
                    }   
                }
            }
            else if (command == nameof(SystemCommands.Commands.OPT))
            {
                foreach (var state in states)
                {
                    var options = filter.CreateListOfOptionsTypesList(textFile.GetFileInfo(command, state.ToUpper()),
                        state.ToUpper());
                    if (options.Any())
                    {
                        textFile.BuildOptionsSqlScript(options, state.ToUpper());
                    }
                }
            }
            Console.WriteLine("Script build is done.");
        }
    }
}