﻿using System;
using Microsoft.VisualBasic.CompilerServices;

namespace FAPriceApp.Models
{
    public class PropertyTypes
    {
        public string PropertyTypeId { get; set; }
        public string CoverageId { get; set; }
        public string StateId { get; set; }
        public string Fee { get; set; }
        public int YearCoverage { get; set; }
        
        public static string MapProp(string propTypeName)
        {
            switch (propTypeName)
            {
                case "Single Family Home (under 5,000 sq. ft.)":
                    return nameof(PropTypeId.SFH);
                case "Condo (under 5,000 sq. ft.)":
                    return nameof(PropTypeId.CON);
                case "Townhouse":
                    return nameof(PropTypeId.TOW);
                case "Mobile Home":
                    return nameof(PropTypeId.MOB);
                case "SFH (5,000 - 5,999 sq. ft.)":
                    return nameof(PropTypeId.SFHo5k);
                case "SFH (over 5,999 sq. ft.) under 10 years":
                    return nameof(PropTypeId.SFHo5ku10);
                case "SFH (over 5,999 sq. ft.) over 10 years":
                    return nameof(PropTypeId.SFHo5ko10);
                case "2-plex":
                    return nameof(PropTypeId.M2);
                case "3-plex":
                    return nameof(PropTypeId.M3);
                case "4-plex":
                    return nameof(PropTypeId.M4);
                case "5-plex":
                    return nameof(PropTypeId.M5);
                case "6-plex":
                    return nameof(PropTypeId.M6);
                case "7-plex":
                    return nameof(PropTypeId.M7);
                case "8-plex":
                    return nameof(PropTypeId.M8);
                case "9-plex":
                    return nameof(PropTypeId.M9);
                case "10-plex":
                    return nameof(PropTypeId.M10);
                case "Guest House (under 750 sq. ft.)":
                    return string.Empty;
                case "Guest House with Options (under 750 sq. ft.)":
                    return string.Empty;
                case "Guest House (over 750 sq. ft.)":
                    return string.Empty;
                case "Guest House with Options (over 750 sq. ft.)":
                    return string.Empty;
                case "Service Fee":
                    return nameof(PropTypeId.ServiceFee);
                default:
                    throw new Exception($"Something wasn't mapped correctly, id {propTypeName}");
            }
        }
    }

    public class FeeStructure
    {
        public string BasicOne { get; set; }
        public string BasicTwo { get; set; }
        public string ValueOne { get; set; }
        public string ValueTwo { get; set; }
        public string EaglePreOne { get; set; }
        public string EaglePreTwo { get; set; }
    }

    public class FeeOptionStructure
    {
        public string BasicOneUnder5k { get; set; }
        public string BasicOneOver5k { get; set; }
        public string BasicTwoUnder5k { get; set; }
        public string BasicTwoOver5k { get; set; }
        public string BasicOneMulti { get; set; }
        public string BasicTwoMulti { get; set; }
        public string ValueOneUnder5k { get; set; }
        public string ValueTwoUnder5k { get; set; }
        public string EaglePreOneUnder5k { get; set; }
        public string EaglePreTwoUnder5k { get; set; }
    }

    public enum PropTypeId
    {
        SFH,
        CON,
        TOW,
        MOB,
        SFHo5k,
        SFHo5ku10,
        SFHo5ko10,
        M2,
        M3,
        M4,
        M5,
        M6,
        M7,
        M8,
        M9,
        M10,
        GHu750,
        GHwOptu750,
        GHo750,
        GHwOpto750,
        ServiceFee
    }
}