﻿using System;

namespace FAPriceApp.Models
{
    public static class Global
    {
        public static string ReferenceFile { get; set; }
        public static string Database { get; set; }
    }
    public static class SystemCommands
    {
        public static void ListCommands()
        {
            Console.WriteLine(nameof(SystemCommands.Commands.OPT));
            Console.WriteLine(nameof(SystemCommands.Commands.PROP));
            Console.WriteLine(nameof(SystemCommands.Commands.SELLER));
        }
        public enum Commands
        {
            PROP,
            OPT,
            SELLER
        }
    }
}