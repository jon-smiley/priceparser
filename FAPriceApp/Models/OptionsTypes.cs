﻿namespace FAPriceApp.Models
{
    public class OptionsTypes
    {
        public string OptionId { get; set; }
        public string PropertyTypeCategory { get; set; } 
        public string CoverageId { get; set; }
        public string StateId { get; set; }
        public string Fee { get; set; }
        public int YearCoverage { get; set; }
        public bool IsFeeByUnit { get; set; }
        public bool IsIncluded { get; set; }
    }
}