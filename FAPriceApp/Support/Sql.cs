﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using FAPriceApp.Models;
using FAPriceApp.Services;

namespace FAPriceApp.Support
{
    public class Sql
    {
        private string[] ListOfExistingStates = new List<string> {"FL", "TN", "GA", "VA", "NV", "WA", "TX"}.ToArray();
        public string GetFileInfo(string type, string stateId)
        {
            var fullPath = @$"{Global.ReferenceFile}\{type}\{stateId}-{type}.txt";
            var txtFile = string.Empty;
            if (!File.Exists(fullPath))
            {
                throw new Exception($"File doesn't exist {fullPath}");
            }

            using (var stream = new FileStream(fullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                var read = new StreamReader(stream);
                txtFile = read.ReadToEnd();
                read.Close();
                stream.Close();
                stream.Dispose();
            }

            return txtFile;
        }

        private string BuildServiceFeeSqlScript(PropertyTypes item)
        {
            var sql = new StringBuilder();
            if (item.YearCoverage > 1 || !ListOfExistingStates.Contains(item.StateId))
            {
                //Coverages with two plus years need to be added
                sql.Append($"INSERT INTO {Global.Database}.[tblFirstAmericanServiceFees]");
                sql.Append(Environment.NewLine);
                sql.Append("([CoverageId],[StateId],[Fee],[YearCoverage])");
                sql.Append(Environment.NewLine);
                sql.Append("VALUES ");
                sql.Append($"('{item.CoverageId}', '{item.StateId}', {item.Fee}, {item.YearCoverage})");
                sql.Append(Environment.NewLine);
                sql.Append("GO");
                sql.Append(Environment.NewLine);
                return sql.ToString();
            }
            //Coverages with One year only need to be updated.
            sql.Append($"UPDATE {Global.Database}.[tblFirstAmericanServiceFees]");
            sql.Append(Environment.NewLine);
            sql.Append($"SET Fee = '{item.Fee}'");
            sql.Append(Environment.NewLine);
            sql.Append($"WHERE CoverageId = '{item.CoverageId}' AND StateId = '{item.StateId}' AND YearCoverage = '{item.YearCoverage}'");
            sql.Append(Environment.NewLine);
            sql.Append("GO");
            sql.Append(Environment.NewLine);
            return sql.ToString();
        }

        public void BuildPropSqlScript(List<PropertyTypes> items, string stateId)
        {
            var newFile = Path.Combine(Global.ReferenceFile, @$"PROP-SCRIPT\{stateId.ToUpper()}-Prop-Script-{Guid.NewGuid()}.sql");
            Console.WriteLine($"New file created: {newFile}");
            if (!Directory.Exists(Path.Combine(Global.ReferenceFile, "PROP-SCRIPT")))
            {
                Directory.CreateDirectory(Path.Combine(Global.ReferenceFile, "PROP-SCRIPT"));
            }

            using (var newStream = File.Create(newFile))
            {
                newStream.Dispose();
            }
            using (var stream = new FileStream(newFile, FileMode.Open, FileAccess.Write, FileShare.ReadWrite))
            {
                var write = new StreamWriter(stream);
                write.WriteLine($"--Script for Property types stateId:{stateId}");
                Console.WriteLine("Script is being built.");
                write.WriteLine(Environment.NewLine);
                foreach (var item in items)
                {
                    if (item.PropertyTypeId == nameof(PropTypeId.ServiceFee))
                    {
                        write.WriteLine(BuildServiceFeeSqlScript(item));
                        continue;;
                    }
                    write.WriteLine(BuildSqlCoverageBasedOnYear(item));
                }
                Console.WriteLine($"Total of {items.Count} updates have been added.");
                write.WriteLine($"--END of prop script for {stateId}");
                write.Dispose();
                stream.Dispose();
            }
        }

        private string BuildSqlCoverageBasedOnYear(PropertyTypes item)
        {
            var sql = new StringBuilder();
            if (item.YearCoverage > 1 || !ListOfExistingStates.Contains(item.StateId))
            {
                //Coverages with two plus years need to be added
                sql.Append($"INSERT INTO {Global.Database}.[tblFirstAmericanCoverageChoices]");
                sql.Append(Environment.NewLine);
                sql.Append("([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])");
                sql.Append(Environment.NewLine);
                sql.Append("VALUES ");
                sql.Append($"('{item.CoverageId}', '{item.PropertyTypeId}', '{item.StateId}', {item.Fee}, {item.YearCoverage})");
                sql.Append(Environment.NewLine);
                sql.Append("GO");
                sql.Append(Environment.NewLine);
                return sql.ToString();
            }
            //Coverages with One year only need to be updated.
            sql.Append($"UPDATE {Global.Database}.[tblFirstAmericanCoverageChoices]");
            sql.Append(Environment.NewLine);
            sql.Append($"SET Fee = '{item.Fee}'");
            sql.Append(Environment.NewLine);
            sql.Append($"WHERE CoverageId = '{item.CoverageId}' AND PropertyTypeId = '{item.PropertyTypeId}' AND StateId = '{item.StateId}' AND YearCoverage = '{item.YearCoverage}'");
            sql.Append(Environment.NewLine);
            sql.Append("GO");
            sql.Append(Environment.NewLine);
            return sql.ToString();
        }
        
        public void BuildOptionsSqlScript(List<OptionsTypes> items, string stateId, bool isSellerCoverage = false)
        {
            var newFile = string.Empty;
            if (isSellerCoverage)
            {
                newFile = Path.Combine(Global.ReferenceFile, @$"SELLER-SCRIPT\{stateId.ToUpper()}-Seller-Script-{Guid.NewGuid()}.sql");
                Console.WriteLine($"New file created: {newFile}");
                if (!Directory.Exists(Path.Combine(Global.ReferenceFile, "SELLER-SCRIPT")))
                {
                    Directory.CreateDirectory(Path.Combine(Global.ReferenceFile, "SELLER-SCRIPT"));
                }
            }
            else
            {
                newFile = Path.Combine(Global.ReferenceFile, @$"OPTION-SCRIPT\{stateId.ToUpper()}-Opt-Script-{Guid.NewGuid()}.sql");
                Console.WriteLine($"New file created: {newFile}");
                if (!Directory.Exists(Path.Combine(Global.ReferenceFile, "OPTION-SCRIPT")))
                {
                    Directory.CreateDirectory(Path.Combine(Global.ReferenceFile, "OPTION-SCRIPT"));
                }
            }
            

            using (var newStream = File.Create(newFile))
            {
                newStream.Dispose();
            }
            using (var stream = new FileStream(newFile, FileMode.Open, FileAccess.Write, FileShare.ReadWrite))
            {
                var write = new StreamWriter(stream);
                write.WriteLine($"--Script for Option types stateId:{stateId}");
                Console.WriteLine("Script is being built.");
                write.WriteLine(Environment.NewLine);
                foreach (var item in items)
                {
                    write.WriteLine(BuildSqlOptionBasedOnYear(item, isSellerCoverage));
                }
                Console.WriteLine($"Total of {items.Count} updates have been added.");
                write.WriteLine($"--END of prop script for {stateId}");
                write.Dispose();
                stream.Dispose();
            }
        }
        
        private string BuildSqlOptionBasedOnYear(OptionsTypes item, bool isSellerCoverage)
        {
            var createNew = new StringBuilder();
            var updateExisting = new StringBuilder();
          
            //Coverages with two plus years need to be added
            createNew.Append($"INSERT INTO {Global.Database}.[tblFirstAmericanOptionChoices]"); 
            createNew.Append(Environment.NewLine); 
            createNew.Append("([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])"); 
            createNew.Append(Environment.NewLine); 
            createNew.Append("VALUES ");
            createNew.Append($"('{item.OptionId.Trim()}', '{item.PropertyTypeCategory}', ");
            createNew.Append($"'{item.CoverageId}', '{item.StateId}', {item.Fee}, {(item.IsFeeByUnit ? 1 : 0)},");
            createNew.Append($"{(item.IsIncluded ? 1 : 0)}, '{(isSellerCoverage && item.OptionId.Trim() == "Sellers First Class Upgrade" ? "First Class Upgrade" : "")}', '', {item.YearCoverage})");
            createNew.Append(Environment.NewLine); 
            
            //Coverages with One year only need to be updated.
            updateExisting.Append($"UPDATE {Global.Database}.[tblFirstAmericanOptionChoices]");
            updateExisting.Append(Environment.NewLine);
            updateExisting.Append($"SET Fee = '{item.Fee}'");
            if (item.IsIncluded)
            {
                updateExisting.Append($", IsIncluded = 1");
            }

            if (isSellerCoverage && item.OptionId.Trim() == "Sellers First Class Upgrade")
            {
                updateExisting.Append($", IncludedConditions = 'First Class Upgrade'");
            }
            updateExisting.Append(Environment.NewLine);
            updateExisting.Append($"WHERE CoverageId = '{item.CoverageId}' AND OptionId = '{item.OptionId.Trim()}' ");
            updateExisting.Append($"AND PropertyTypeCategory = '{item.PropertyTypeCategory}' ");
            updateExisting.Append($"AND StateId = '{item.StateId}' ");
            updateExisting.Append($"AND YearCoverage = {item.YearCoverage}");
            updateExisting.Append(Environment.NewLine);

            var existsStatement = @$"
            IF EXISTS (
                SELECT * FROM {Global.Database}.[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = '{item.CoverageId}' 
                    AND OptionId = '{item.OptionId.Trim()}' 
                    AND PropertyTypeCategory = '{item.PropertyTypeCategory}'
                    AND StateId = '{item.StateId}'
                    AND YearCoverage = '{item.YearCoverage}'
            )
            BEGIN
                {updateExisting.ToString()}
            END
            ELSE
            BEGIN
                {createNew.ToString()}
            END
            GO
            {Environment.NewLine}
            ";
            return existsStatement;
        }
    }
}