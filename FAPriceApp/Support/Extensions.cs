﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using FAPriceApp.Models;
using FAPriceApp.Services;

namespace FAPriceApp.Support
{
    public static class Extensions
    {
        public static bool HasValidFee(this OptionsTypes opt)
        {
            return opt.Fee != Filter.NotAc && opt.Fee != Filter.Inc && !string.IsNullOrEmpty(opt.Fee);
        }
        
        public static bool HasValidFee(this PropertyTypes opt)
        {
            return opt.Fee != Filter.NotAc && !string.IsNullOrEmpty(opt.Fee);
        }

        public static OptionsTypes TryAddToList(this OptionsTypes opt, List<OptionsTypes> list)
        {
            if (opt.HasValidFee())
            {
                list.Add(opt);
            }

            return opt;
        }
        
        public static PropertyTypes TryAddToList(this PropertyTypes opt, List<PropertyTypes> list)
        {
            if (opt.HasValidFee())
            {
                list.Add(opt);
            }

            return opt;
        }
    }
}