
UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptions]
SET OptionId = 'HVAC Tune-Up', Comments = '', OptionName = 'HVAC Tune-Up'
WHERE OptionId = 'HVAC Tune Up ($35 Value)'

select * from tblFirstAmericanOptionChoices
where stateId in ('tn') and OptionId like '%sellers h%'

--need to run this in stg
--delete from tblFirstAmericanOptionChoices
--where stateId = 'tn' and OptionId = 'Sellers HG/AC/Duct Package'

--Add script to put TN tblFirstAmericanServiceFees back
--delete from tblFirstAmericanServiceFees
--where StateId = 'tn'

select * from tblFirstAmericanServiceFees where StateId = 'tx'

--update tblfirstamericanservicefees
--set YearCoverage = 1
--where UIDServiceFee = 51

select * from tblFirstAmericanOptions where optionId = 'Sellers AC/Duct Package'

--delete from tblFirstAmericanOptionChoices where stateid = 'nv' and OptionId = 'Sellers HG/AC/Duct Package'

SELECT CONCAT(coverages.CoverageName, ' ', choices.YearCoverage, ' Year') AS CoverageName, 
						CONCAT(coverages.CoverageNameSpanish, ' ', choices.YearCoverage, ' A�o') AS CoverageNameSpanish, 
                        choices.Fee, 
                        coverages.CoverageId,
                        choices.YearCoverage,
						fees.Fee AS ServiceFee
                        FROM tblFirstAmericanCoverages coverages WITH (NOLOCK)
                        INNER JOIN tblFirstAmericanCoverageChoices choices WITH (NOLOCK)
                        ON coverages.CoverageId = choices.CoverageId
						left JOIN tblFirstAmericanServiceFees fees with (nolock)
						on coverages.CoverageId = fees.CoverageId 
							and choices.StateId = fees.StateId 
							and choices.YearCoverage = fees.YearCoverage
                        WHERE choices.StateId = 'wa'
                        AND choices.PropertyTypeId = 'SFH'
                        ORDER BY coverages.DisplayOrder

SELECT options.OptionName,
                            choices.IncludedConditions,
                            choices.IsIncluded, 
                            options.OptionCategory, 
                            translation.OptionNameSpanish,
							options.*,
                            CASE WHEN choices.IsFeeByUnit = 1
                            THEN choices.Fee * 1
                            ELSE choices.Fee
                            END as [Fee]
                        FROM tblFirstAmericanOptions options WITH (NOLOCK)
                        INNER JOIN tblFirstAmericanOptionChoices choices WITH (NOLOCK)
                            ON options.OptionId=choices.OptionId
                        LEFT JOIN tblFirstAmericanOptionsTranslations translation WITH (NOLOCK)
		                    ON options.OptionId = translation.OptionId
                        WHERE choices.StateId = 'wa'
                        AND choices.PropertyTypeCategory = 'UNDER5000'
                        AND choices.CoverageId = 'Basic'
                        AND choices.YearCoverage = 1
						and options.OptionName like '%sellers%'
                        ORDER BY options.DisplayOrder


select * from tblFirstAmericanOptionChoices where OptionId like '%sellers%' and StateId = 'wa'

select * from tblFirstAmericanOptions where UIDOption in ('20', '22', '21', '23')

	Select *
	FROM INFORMATION_SCHEMA.TABLES
	WHERE TABLE_NAME = 'tblTransactionCmaReports'


