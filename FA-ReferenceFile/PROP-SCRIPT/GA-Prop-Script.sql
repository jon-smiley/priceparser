--Script for Property types stateId:GA


UPDATE [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
SET Fee = '75'
WHERE CoverageId = 'Basic' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'GA', 75, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
SET Fee = '75'
WHERE CoverageId = 'Eagle Premier' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'GA', 75, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '430'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFH' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFH', 'GA', 795, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '560'
WHERE CoverageId = 'Eagle Premier' AND PropertyTypeId = 'SFH' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'SFH', 'GA', 1035, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '380'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'CON' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'CON', 'GA', 705, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '495'
WHERE CoverageId = 'Eagle Premier' AND PropertyTypeId = 'CON' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'CON', 'GA', 915, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '860'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFHo5k' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5k', 'GA', 1590, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '860'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFHo5ku10' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ku10', 'GA', 1590, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '960'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFHo5ko10' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ko10', 'GA', 1775, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '600'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M2' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M2', 'GA', 1200, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '905'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M3' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M3', 'GA', 1810, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1205'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M4' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M4', 'GA', 2410, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1505'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M5' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M5', 'GA', 3010, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1805'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M6' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M6', 'GA', 3610, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '2105'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M7' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M7', 'GA', 4210, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '2410'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M8' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M8', 'GA', 4820, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '2710'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M9' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M9', 'GA', 5420, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '3010'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M10' AND StateId = 'GA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M10', 'GA', 6020, 2)
GO

--END of prop script for GA
