--Script for Property types stateId:VA


UPDATE [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
SET Fee = '100'
WHERE CoverageId = 'Basic' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'VA', 100, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
SET Fee = '100'
WHERE CoverageId = 'Value Plus' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'VA', 100, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
SET Fee = '100'
WHERE CoverageId = 'Eagle Premier' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'VA', 100, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '430'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFH' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFH', 'VA', 795, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '540'
WHERE CoverageId = 'Value Plus' AND PropertyTypeId = 'SFH' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'SFH', 'VA', 1000, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '605'
WHERE CoverageId = 'Eagle Premier' AND PropertyTypeId = 'SFH' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'SFH', 'VA', 1120, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '380'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'CON' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'CON', 'VA', 705, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '475'
WHERE CoverageId = 'Value Plus' AND PropertyTypeId = 'CON' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'CON', 'VA', 880, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '530'
WHERE CoverageId = 'Eagle Premier' AND PropertyTypeId = 'CON' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'CON', 'VA', 980, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '860'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFHo5k' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5k', 'VA', 1590, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '860'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFHo5ku10' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ku10', 'VA', 1590, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '960'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFHo5ko10' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ko10', 'VA', 1775, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '600'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M2' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M2', 'VA', 1200, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '905'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M3' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M3', 'VA', 1810, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1205'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M4' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M4', 'VA', 2410, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1505'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M5' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M5', 'VA', 3010, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1805'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M6' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M6', 'VA', 3610, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '2105'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M7' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M7', 'VA', 4210, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '2410'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M8' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M8', 'VA', 4820, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '2710'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M9' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M9', 'VA', 5420, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '3010'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M10' AND StateId = 'VA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M10', 'VA', 6020, 2)
GO

--END of prop script for VA
