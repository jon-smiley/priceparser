--Script for Property types stateId:TN


UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '455'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFH' AND StateId = 'TN' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFH', 'TN', 840, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '630'
WHERE CoverageId = 'Eagle Premier' AND PropertyTypeId = 'SFH' AND StateId = 'TN' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'SFH', 'TN', 1165, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '400'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'CON' AND StateId = 'TN' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'CON', 'TN', 740, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '555'
WHERE CoverageId = 'Eagle Premier' AND PropertyTypeId = 'CON' AND StateId = 'TN' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'CON', 'TN', 1025, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '910'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFHo5k' AND StateId = 'TN' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5k', 'TN', 1685, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '910'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFHo5ku10' AND StateId = 'TN' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ku10', 'TN', 1685, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1010'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFHo5ko10' AND StateId = 'TN' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ko10', 'TN', 1870, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '635'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M2' AND StateId = 'TN' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M2', 'TN', 1270, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '955'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M3' AND StateId = 'TN' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M3', 'TN', 1910, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1275'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M4' AND StateId = 'TN' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M4', 'TN', 2550, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1595'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M5' AND StateId = 'TN' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M5', 'TN', 3190, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1910'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M6' AND StateId = 'TN' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M6', 'TN', 3820, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '2230'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M7' AND StateId = 'TN' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M7', 'TN', 4460, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '2550'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M8' AND StateId = 'TN' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M8', 'TN', 5100, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '2865'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M9' AND StateId = 'TN' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M9', 'TN', 5730, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '3185'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M10' AND StateId = 'TN' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M10', 'TN', 6370, 2)
GO

--END of prop script for TN
