--Script for Property types stateId:WA


UPDATE [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
SET Fee = '75'
WHERE CoverageId = 'Basic' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'WA', 75, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
SET Fee = '75'
WHERE CoverageId = 'Value Plus' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'WA', 75, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
SET Fee = '75'
WHERE CoverageId = 'Eagle Premier' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'WA', 75, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '310'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFH' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFH', 'WA', 575, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '460'
WHERE CoverageId = 'Value Plus' AND PropertyTypeId = 'SFH' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'SFH', 'WA', 850, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '550'
WHERE CoverageId = 'Eagle Premier' AND PropertyTypeId = 'SFH' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'SFH', 'WA', 1020, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '275'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'CON' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'CON', 'WA', 510, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '405'
WHERE CoverageId = 'Value Plus' AND PropertyTypeId = 'CON' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'CON', 'WA', 750, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '485'
WHERE CoverageId = 'Eagle Premier' AND PropertyTypeId = 'CON' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'CON', 'WA', 895, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '620'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFHo5k' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5k', 'WA', 1145, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '620'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFHo5ku10' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ku10', 'WA', 1145, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '720'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFHo5ko10' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ko10', 'WA', 1330, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '435'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M2' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M2', 'WA', 870, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '650'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M3' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M3', 'WA', 1300, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '870'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M4' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M4', 'WA', 1740, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1085'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M5' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M5', 'WA', 2170, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1300'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M6' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M6', 'WA', 2600, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1520'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M7' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M7', 'WA', 3040, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1735'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M8' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M8', 'WA', 3470, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1955'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M9' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M9', 'WA', 3910, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '2170'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M10' AND StateId = 'WA' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M10', 'WA', 4340, 2)
GO

--END of prop script for WA
