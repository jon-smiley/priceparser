--Script for Property types stateId:NM


INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'NM', 75, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'NM', 75, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'NM', 75, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'NM', 75, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFH', 'NM', 415, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFH', 'NM', 770, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'SFH', 'NM', 590, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'SFH', 'NM', 1090, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'CON', 'NM', 365, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'CON', 'NM', 675, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'CON', 'NM', 520, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'CON', 'NM', 960, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5k', 'NM', 830, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5k', 'NM', 1535, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ku10', 'NM', 830, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ku10', 'NM', 1535, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ko10', 'NM', 930, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ko10', 'NM', 1720, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M2', 'NM', 580, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M2', 'NM', 1160, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M3', 'NM', 870, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M3', 'NM', 1740, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M4', 'NM', 1160, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M4', 'NM', 2320, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M5', 'NM', 1455, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M5', 'NM', 2910, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M6', 'NM', 1745, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M6', 'NM', 3490, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M7', 'NM', 2035, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M7', 'NM', 4070, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M8', 'NM', 2325, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M8', 'NM', 4650, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M9', 'NM', 2615, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M9', 'NM', 5230, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M10', 'NM', 2905, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M10', 'NM', 5810, 2)
GO

--END of prop script for NM
