﻿<h2>Price Parser</h2>
This application is meant to help parse out the FirstAmerican Warranty price from an excel sheet to script.</br></br>

<i>Workflow</i>
---------------------
- First question "What is the folder you want to reference?". This will reference the csv files necessary to build the scripts.
- Next question "What database are you referencing. Example [TransactionDesk].[dbo]". Provide which database you would like to reference. The table names match the ones provided in the TD-FirstAmericanSchema.
- Next question "What command are you preforming.". You have the option of PROP, OPT, SELLER. Each corresponds to a table in the FA excel sheet.
- The final question "Add states associated with command. Or Exit." Add a list of states that you want to generate scripts for.

<i>Things to keep in mind</i>
-----------------------------------------
- The application will first ask you for a reference file, in the reference file there should be csv versions of the pricing excel file under the appropriate folder. The folder names should be SELLER, OPT, PROP. 
- Convert each table in the excel to a general format (meaning no symbols or decimals) and then to a csv file. Each entry must begin and end with a $ symbol. This is how the application parses out each row. (Attached is an example of the correct format.)
- Once run, the application will create a new file that corresponds with the command entered.
- EX: If the command entered was PROP, then the application will create a new folder called PROP-SCRIPT with the generated sql script.
- Each generated script will have a new Guid in the name of the file. This is to keep track of scripts generated and can be removed when added to a branch.
- Make sure that your csv file follows this format. StateId-CommandType and is a txt file.

<i>Notes</i>
-------------------
<p>This simple application is meant to make parsing prices for FA much easier than going row by row. If you have an idea on how to make it better or other ways to utilitze this, please make a pr and contribute.</p>