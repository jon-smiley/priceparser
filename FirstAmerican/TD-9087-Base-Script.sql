--States
print N'Update [tblFirstAmericanStates]'
INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanStates](StateId, StateName, Website)
VALUES
	('CO', 'Colorado', ''),
	('KY', 'Kentucky', ''),
	('OH', 'Ohio', ''),
	('NM', 'New Mexico', ''),
	('OK', 'Oklahoma', ''),
	('AZ', 'Arizona', '')
GO
print N'Done Updating States'

print N'Add YearCoverage to Coverage Choices'
ALTER TABLE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
ADD YearCoverage int NOT NULL Default 1
GO

print N'Add YearCoverage to Option Choices'
ALTER TABLE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
ADD YearCoverage int NOT NULL Default 1
GO

print N'Add YearCoverage to Service Fees'
ALTER TABLE [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
ADD YearCoverage int NOT NULL Default 1
GO

print N'Add new states to Property types'
UPDATE [TransactionDesk].[dbo].[tblFirstAmericanPropertyTypes]
SET StateIds = 'FL,GA,NV,TN,TX,VA,WA,CO,KY,OH,NM,OK,AZ'
WHERE PropertyTypeId IN ('SFH','CON','SFHo5k','SFHo5ku10','SFHo5ko10','M2','M3','M4','M5','M6','M7','M8','M9','M10')
GO

print N'Update HVAC Tune Up ($35 Value) to HVAC Tune Up to make Option Id match source.'
UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET OptionId = 'HVAC Tune-Up', Comments = ''
WHERE OptionId = 'HVAC Tune Up ($35 Value)'
GO

print N'Update HVAC Tune Up ($35 Value) to HVAC Tune Up to make Option Id match source. (For tblFirstAmericanOptions)'
UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptions]
SET OptionId = 'HVAC Tune-Up', Comments = '', OptionName = 'HVAC Tune-Up'
WHERE OptionId = 'HVAC Tune Up ($35 Value)'
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionsTranslations]
SET OptionId = 'HVAC Tune-Up', OptionNameSpanish = 'HVAC Tune-Up'
WHERE OptionId = 'HVAC Tune Up ($35 Value)'
GO

--Update existing TN values for Value plus plan
UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET PropertyTypeCategory = 'UNDER5000'
WHERE StateId = 'TN' AND CoverageId = 'Value Plus'
GO

--Remove previous option choices for TX
DELETE FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
WHERE OptionId = 'Sellers Heating/AC/Duct Package' AND StateId = 'TX'
GO

--Removing Sellers AC/Duct Package from WA options
DELETE FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
WHERE OptionId = 'Sellers AC/Duct Package'
GO

DELETE FROM [TransactionDesk].[dbo].[tblFirstAmericanOptions]
WHERE OptionId = 'Sellers AC/Duct Package'
GO

DELETE FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionsTranslations]
WHERE OptionId = 'Sellers AC/Duct Package'
GO

--Adding new options
INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptions](OptionId, OptionName, OptionCategory, DisplayOrder, Comments)
VALUES('Sellers HG/AC/Duct Package', 'Sellers HG/AC/Duct Package', 'SELLER', 13, '')
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptions](OptionId, OptionName, OptionCategory, DisplayOrder, Comments)
VALUES('Sellers First Class Upgrade', 'Sellers First Class Upgrade', 'SELLER', 14, '')
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptions](OptionId, OptionName, OptionCategory, DisplayOrder, Comments)
VALUES('Plumbing Plus', 'Plumbing Plus', 'BUYER', 15, '')
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptions](OptionId, OptionName, OptionCategory, DisplayOrder, Comments)
VALUES('First Class Plus', 'First Class Plus', 'BUYER', 16, '')
GO

