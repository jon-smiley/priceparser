--Script for Property types stateId:TX
--Had to add Service fees for TX, looks like they were not added when tables were initally made.
INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'TX', 75, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'TX', 75, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'TX', 75, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'TX', 75, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '405'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFH' AND StateId = 'TX' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFH', 'TX', 750, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '610'
WHERE CoverageId = 'Eagle Premier' AND PropertyTypeId = 'SFH' AND StateId = 'TX' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'SFH', 'TX', 1130, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '355'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'CON' AND StateId = 'TX' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'CON', 'TX', 655, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '535'
WHERE CoverageId = 'Eagle Premier' AND PropertyTypeId = 'CON' AND StateId = 'TX' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'CON', 'TX', 990, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '810'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFHo5k' AND StateId = 'TX' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5k', 'TX', 1500, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '810'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFHo5ku10' AND StateId = 'TX' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ku10', 'TX', 1500, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '910'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'SFHo5ko10' AND StateId = 'TX' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ko10', 'TX', 1685, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '565'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M2' AND StateId = 'TX' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M2', 'TX', 1130, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '850'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M3' AND StateId = 'TX' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M3', 'TX', 1700, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1135'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M4' AND StateId = 'TX' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M4', 'TX', 2270, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1420'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M5' AND StateId = 'TX' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M5', 'TX', 2840, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1700'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M6' AND StateId = 'TX' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M6', 'TX', 3400, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '1985'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M7' AND StateId = 'TX' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M7', 'TX', 3970, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '2270'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M8' AND StateId = 'TX' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M8', 'TX', 4540, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '2550'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M9' AND StateId = 'TX' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M9', 'TX', 5100, 2)
GO

UPDATE [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
SET Fee = '2835'
WHERE CoverageId = 'Basic' AND PropertyTypeId = 'M10' AND StateId = 'TX' AND YearCoverage = '1'
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M10', 'TX', 5670, 2)
GO

--END of prop script for TX
