--Script for Property types stateId:AZ


INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'AZ', 75, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'AZ', 75, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'AZ', 75, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'AZ', 75, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'AZ', 75, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'AZ', 75, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFH', 'AZ', 390, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFH', 'AZ', 720, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'SFH', 'AZ', 510, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'SFH', 'AZ', 945, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'SFH', 'AZ', 560, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'SFH', 'AZ', 1035, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'CON', 'AZ', 345, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'CON', 'AZ', 640, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'CON', 'AZ', 450, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'CON', 'AZ', 835, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'CON', 'AZ', 495, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'CON', 'AZ', 915, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5k', 'AZ', 780, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5k', 'AZ', 1445, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ku10', 'AZ', 780, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ku10', 'AZ', 1445, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ko10', 'AZ', 880, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ko10', 'AZ', 1630, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M2', 'AZ', 545, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M2', 'AZ', 1090, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M3', 'AZ', 820, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M3', 'AZ', 1640, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M4', 'AZ', 1090, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M4', 'AZ', 2180, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M5', 'AZ', 1365, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M5', 'AZ', 2730, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M6', 'AZ', 1640, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M6', 'AZ', 3280, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M7', 'AZ', 1910, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M7', 'AZ', 3820, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M8', 'AZ', 2185, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M8', 'AZ', 4370, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M9', 'AZ', 2455, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M9', 'AZ', 4910, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M10', 'AZ', 2730, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M10', 'AZ', 5460, 2)
GO

--END of prop script for AZ
