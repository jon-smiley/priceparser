--Script for Property types stateId:KY


INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'KY', 75, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'KY', 75, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'KY', 75, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'KY', 75, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'KY', 75, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanServiceFees]
([CoverageId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'KY', 75, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFH', 'KY', 405, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFH', 'KY', 750, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'SFH', 'KY', 515, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'SFH', 'KY', 955, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'SFH', 'KY', 580, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'SFH', 'KY', 1075, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'CON', 'KY', 355, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'CON', 'KY', 655, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'CON', 'KY', 455, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Value Plus', 'CON', 'KY', 840, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'CON', 'KY', 510, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Eagle Premier', 'CON', 'KY', 945, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5k', 'KY', 810, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5k', 'KY', 1500, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ku10', 'KY', 810, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ku10', 'KY', 1500, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ko10', 'KY', 910, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'SFHo5ko10', 'KY', 1685, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M2', 'KY', 565, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M2', 'KY', 1130, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M3', 'KY', 850, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M3', 'KY', 1700, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M4', 'KY', 1135, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M4', 'KY', 2270, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M5', 'KY', 1420, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M5', 'KY', 2840, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M6', 'KY', 1700, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M6', 'KY', 3400, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M7', 'KY', 1985, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M7', 'KY', 3970, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M8', 'KY', 2270, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M8', 'KY', 4540, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M9', 'KY', 2550, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M9', 'KY', 5100, 2)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M10', 'KY', 2835, 1)
GO

INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanCoverageChoices]
([CoverageId],[PropertyTypeId],[StateId],[Fee],[YearCoverage])
VALUES ('Basic', 'M10', 'KY', 5670, 2)
GO

--END of prop script for KY
