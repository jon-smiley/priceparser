--Script for Option types stateId:CO



            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'First Class Upgrade' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '100'
WHERE CoverageId = 'Basic' AND OptionId = 'First Class Upgrade' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('First Class Upgrade', 'UNDER5000', 'Basic', 'CO', 100, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'First Class Upgrade' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '150'
WHERE CoverageId = 'Basic' AND OptionId = 'First Class Upgrade' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('First Class Upgrade', 'OVER5000', 'Basic', 'CO', 150, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'First Class Upgrade' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '200'
WHERE CoverageId = 'Basic' AND OptionId = 'First Class Upgrade' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('First Class Upgrade', 'UNDER5000', 'Basic', 'CO', 200, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'First Class Upgrade' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '300'
WHERE CoverageId = 'Basic' AND OptionId = 'First Class Upgrade' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('First Class Upgrade', 'OVER5000', 'Basic', 'CO', 300, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'First Class Upgrade' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '100'
WHERE CoverageId = 'Basic' AND OptionId = 'First Class Upgrade' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('First Class Upgrade', 'MULTIPLEX', 'Basic', 'CO', 100, 1,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'First Class Upgrade' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '200'
WHERE CoverageId = 'Basic' AND OptionId = 'First Class Upgrade' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('First Class Upgrade', 'MULTIPLEX', 'Basic', 'CO', 200, 1,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'First Class Upgrade' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Value Plus' AND OptionId = 'First Class Upgrade' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('First Class Upgrade', 'UNDER5000', 'Value Plus', 'CO', 0, 0,1, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'First Class Upgrade' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Value Plus' AND OptionId = 'First Class Upgrade' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('First Class Upgrade', 'UNDER5000', 'Value Plus', 'CO', 0, 0,1, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'First Class Upgrade' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'First Class Upgrade' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('First Class Upgrade', 'UNDER5000', 'Eagle Premier', 'CO', 0, 0,1, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'First Class Upgrade' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'First Class Upgrade' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('First Class Upgrade', 'UNDER5000', 'Eagle Premier', 'CO', 0, 0,1, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Central Air Conditioning' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '85'
WHERE CoverageId = 'Basic' AND OptionId = 'Central Air Conditioning' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Central Air Conditioning', 'UNDER5000', 'Basic', 'CO', 85, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Central Air Conditioning' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '130'
WHERE CoverageId = 'Basic' AND OptionId = 'Central Air Conditioning' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Central Air Conditioning', 'OVER5000', 'Basic', 'CO', 130, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Central Air Conditioning' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '170'
WHERE CoverageId = 'Basic' AND OptionId = 'Central Air Conditioning' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Central Air Conditioning', 'UNDER5000', 'Basic', 'CO', 170, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Central Air Conditioning' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '260'
WHERE CoverageId = 'Basic' AND OptionId = 'Central Air Conditioning' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Central Air Conditioning', 'OVER5000', 'Basic', 'CO', 260, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Central Air Conditioning' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '85'
WHERE CoverageId = 'Basic' AND OptionId = 'Central Air Conditioning' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Central Air Conditioning', 'MULTIPLEX', 'Basic', 'CO', 85, 1,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Central Air Conditioning' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '170'
WHERE CoverageId = 'Basic' AND OptionId = 'Central Air Conditioning' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Central Air Conditioning', 'MULTIPLEX', 'Basic', 'CO', 170, 1,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Central Air Conditioning' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Value Plus' AND OptionId = 'Central Air Conditioning' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Central Air Conditioning', 'UNDER5000', 'Value Plus', 'CO', 0, 0,1, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Central Air Conditioning' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Value Plus' AND OptionId = 'Central Air Conditioning' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Central Air Conditioning', 'UNDER5000', 'Value Plus', 'CO', 0, 0,1, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Central Air Conditioning' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Central Air Conditioning' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Central Air Conditioning', 'UNDER5000', 'Eagle Premier', 'CO', 0, 0,1, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Central Air Conditioning' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Central Air Conditioning' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Central Air Conditioning', 'UNDER5000', 'Eagle Premier', 'CO', 0, 0,1, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '50'
WHERE CoverageId = 'Basic' AND OptionId = 'Kitchen Refrigerator' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Kitchen Refrigerator', 'UNDER5000', 'Basic', 'CO', 50, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '75'
WHERE CoverageId = 'Basic' AND OptionId = 'Kitchen Refrigerator' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Kitchen Refrigerator', 'OVER5000', 'Basic', 'CO', 75, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '100'
WHERE CoverageId = 'Basic' AND OptionId = 'Kitchen Refrigerator' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Kitchen Refrigerator', 'UNDER5000', 'Basic', 'CO', 100, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '150'
WHERE CoverageId = 'Basic' AND OptionId = 'Kitchen Refrigerator' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Kitchen Refrigerator', 'OVER5000', 'Basic', 'CO', 150, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '50'
WHERE CoverageId = 'Basic' AND OptionId = 'Kitchen Refrigerator' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Kitchen Refrigerator', 'MULTIPLEX', 'Basic', 'CO', 50, 1,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '100'
WHERE CoverageId = 'Basic' AND OptionId = 'Kitchen Refrigerator' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Kitchen Refrigerator', 'MULTIPLEX', 'Basic', 'CO', 100, 1,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '50'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Kitchen Refrigerator' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Kitchen Refrigerator', 'UNDER5000', 'Value Plus', 'CO', 50, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '100'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Kitchen Refrigerator' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Kitchen Refrigerator', 'UNDER5000', 'Value Plus', 'CO', 100, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Kitchen Refrigerator' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Kitchen Refrigerator', 'UNDER5000', 'Eagle Premier', 'CO', 0, 0,1, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Kitchen Refrigerator' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Kitchen Refrigerator', 'UNDER5000', 'Eagle Premier', 'CO', 0, 0,1, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Washer/Dryer' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '85'
WHERE CoverageId = 'Basic' AND OptionId = 'Washer/Dryer' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer', 'UNDER5000', 'Basic', 'CO', 85, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Washer/Dryer' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '130'
WHERE CoverageId = 'Basic' AND OptionId = 'Washer/Dryer' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer', 'OVER5000', 'Basic', 'CO', 130, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Washer/Dryer' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '170'
WHERE CoverageId = 'Basic' AND OptionId = 'Washer/Dryer' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer', 'UNDER5000', 'Basic', 'CO', 170, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Washer/Dryer' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '260'
WHERE CoverageId = 'Basic' AND OptionId = 'Washer/Dryer' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer', 'OVER5000', 'Basic', 'CO', 260, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Washer/Dryer' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '85'
WHERE CoverageId = 'Basic' AND OptionId = 'Washer/Dryer' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer', 'MULTIPLEX', 'Basic', 'CO', 85, 1,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Washer/Dryer' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '170'
WHERE CoverageId = 'Basic' AND OptionId = 'Washer/Dryer' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer', 'MULTIPLEX', 'Basic', 'CO', 170, 1,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Washer/Dryer' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '85'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Washer/Dryer' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer', 'UNDER5000', 'Value Plus', 'CO', 85, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Washer/Dryer' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '170'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Washer/Dryer' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer', 'UNDER5000', 'Value Plus', 'CO', 170, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Washer/Dryer' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Washer/Dryer' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer', 'UNDER5000', 'Eagle Premier', 'CO', 0, 0,1, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Washer/Dryer' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Washer/Dryer' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer', 'UNDER5000', 'Eagle Premier', 'CO', 0, 0,1, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '120'
WHERE CoverageId = 'Basic' AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer/Kitchen Refrigerator', 'UNDER5000', 'Basic', 'CO', 120, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '180'
WHERE CoverageId = 'Basic' AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer/Kitchen Refrigerator', 'OVER5000', 'Basic', 'CO', 180, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '240'
WHERE CoverageId = 'Basic' AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer/Kitchen Refrigerator', 'UNDER5000', 'Basic', 'CO', 240, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '360'
WHERE CoverageId = 'Basic' AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer/Kitchen Refrigerator', 'OVER5000', 'Basic', 'CO', 360, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '120'
WHERE CoverageId = 'Basic' AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer/Kitchen Refrigerator', 'MULTIPLEX', 'Basic', 'CO', 120, 1,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '240'
WHERE CoverageId = 'Basic' AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer/Kitchen Refrigerator', 'MULTIPLEX', 'Basic', 'CO', 240, 1,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '120'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer/Kitchen Refrigerator', 'UNDER5000', 'Value Plus', 'CO', 120, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '240'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer/Kitchen Refrigerator', 'UNDER5000', 'Value Plus', 'CO', 240, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer/Kitchen Refrigerator', 'UNDER5000', 'Eagle Premier', 'CO', 0, 0,1, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Washer/Dryer/Kitchen Refrigerator' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Washer/Dryer/Kitchen Refrigerator', 'UNDER5000', 'Eagle Premier', 'CO', 0, 0,1, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Additional Refrigeration' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '50'
WHERE CoverageId = 'Basic' AND OptionId = 'Additional Refrigeration' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Additional Refrigeration', 'UNDER5000', 'Basic', 'CO', 50, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Additional Refrigeration' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '75'
WHERE CoverageId = 'Basic' AND OptionId = 'Additional Refrigeration' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Additional Refrigeration', 'OVER5000', 'Basic', 'CO', 75, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Additional Refrigeration' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '100'
WHERE CoverageId = 'Basic' AND OptionId = 'Additional Refrigeration' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Additional Refrigeration', 'UNDER5000', 'Basic', 'CO', 100, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Additional Refrigeration' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '150'
WHERE CoverageId = 'Basic' AND OptionId = 'Additional Refrigeration' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Additional Refrigeration', 'OVER5000', 'Basic', 'CO', 150, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Additional Refrigeration' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '50'
WHERE CoverageId = 'Basic' AND OptionId = 'Additional Refrigeration' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Additional Refrigeration', 'MULTIPLEX', 'Basic', 'CO', 50, 1,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Additional Refrigeration' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '100'
WHERE CoverageId = 'Basic' AND OptionId = 'Additional Refrigeration' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Additional Refrigeration', 'MULTIPLEX', 'Basic', 'CO', 100, 1,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Additional Refrigeration' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '50'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Additional Refrigeration' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Additional Refrigeration', 'UNDER5000', 'Value Plus', 'CO', 50, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Additional Refrigeration' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '100'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Additional Refrigeration' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Additional Refrigeration', 'UNDER5000', 'Value Plus', 'CO', 100, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Additional Refrigeration' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '50'
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Additional Refrigeration' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Additional Refrigeration', 'UNDER5000', 'Eagle Premier', 'CO', 50, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Additional Refrigeration' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '100'
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Additional Refrigeration' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Additional Refrigeration', 'UNDER5000', 'Eagle Premier', 'CO', 100, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Pool/Spa (Includes Saltwater Pool)' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '180'
WHERE CoverageId = 'Basic' AND OptionId = 'Pool/Spa (Includes Saltwater Pool)' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Pool/Spa (Includes Saltwater Pool)', 'UNDER5000', 'Basic', 'CO', 180, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Pool/Spa (Includes Saltwater Pool)' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '270'
WHERE CoverageId = 'Basic' AND OptionId = 'Pool/Spa (Includes Saltwater Pool)' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Pool/Spa (Includes Saltwater Pool)', 'OVER5000', 'Basic', 'CO', 270, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Pool/Spa (Includes Saltwater Pool)' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '360'
WHERE CoverageId = 'Basic' AND OptionId = 'Pool/Spa (Includes Saltwater Pool)' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Pool/Spa (Includes Saltwater Pool)', 'UNDER5000', 'Basic', 'CO', 360, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Pool/Spa (Includes Saltwater Pool)' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '540'
WHERE CoverageId = 'Basic' AND OptionId = 'Pool/Spa (Includes Saltwater Pool)' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Pool/Spa (Includes Saltwater Pool)', 'OVER5000', 'Basic', 'CO', 540, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Pool/Spa (Includes Saltwater Pool)' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '180'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Pool/Spa (Includes Saltwater Pool)' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Pool/Spa (Includes Saltwater Pool)', 'UNDER5000', 'Value Plus', 'CO', 180, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Pool/Spa (Includes Saltwater Pool)' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '360'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Pool/Spa (Includes Saltwater Pool)' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Pool/Spa (Includes Saltwater Pool)', 'UNDER5000', 'Value Plus', 'CO', 360, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Pool/Spa (Includes Saltwater Pool)' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '180'
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Pool/Spa (Includes Saltwater Pool)' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Pool/Spa (Includes Saltwater Pool)', 'UNDER5000', 'Eagle Premier', 'CO', 180, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Pool/Spa (Includes Saltwater Pool)' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '360'
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Pool/Spa (Includes Saltwater Pool)' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Pool/Spa (Includes Saltwater Pool)', 'UNDER5000', 'Eagle Premier', 'CO', 360, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Septic Tank Pumping/System' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '75'
WHERE CoverageId = 'Basic' AND OptionId = 'Septic Tank Pumping/System' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Septic Tank Pumping/System', 'UNDER5000', 'Basic', 'CO', 75, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Septic Tank Pumping/System' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '115'
WHERE CoverageId = 'Basic' AND OptionId = 'Septic Tank Pumping/System' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Septic Tank Pumping/System', 'OVER5000', 'Basic', 'CO', 115, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Septic Tank Pumping/System' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '150'
WHERE CoverageId = 'Basic' AND OptionId = 'Septic Tank Pumping/System' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Septic Tank Pumping/System', 'UNDER5000', 'Basic', 'CO', 150, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Septic Tank Pumping/System' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '230'
WHERE CoverageId = 'Basic' AND OptionId = 'Septic Tank Pumping/System' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Septic Tank Pumping/System', 'OVER5000', 'Basic', 'CO', 230, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Septic Tank Pumping/System' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '75'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Septic Tank Pumping/System' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Septic Tank Pumping/System', 'UNDER5000', 'Value Plus', 'CO', 75, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Septic Tank Pumping/System' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '150'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Septic Tank Pumping/System' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Septic Tank Pumping/System', 'UNDER5000', 'Value Plus', 'CO', 150, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Septic Tank Pumping/System' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '75'
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Septic Tank Pumping/System' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Septic Tank Pumping/System', 'UNDER5000', 'Eagle Premier', 'CO', 75, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Septic Tank Pumping/System' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '150'
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Septic Tank Pumping/System' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Septic Tank Pumping/System', 'UNDER5000', 'Eagle Premier', 'CO', 150, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Well Pump' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '85'
WHERE CoverageId = 'Basic' AND OptionId = 'Well Pump' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Well Pump', 'UNDER5000', 'Basic', 'CO', 85, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Well Pump' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '130'
WHERE CoverageId = 'Basic' AND OptionId = 'Well Pump' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Well Pump', 'OVER5000', 'Basic', 'CO', 130, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Well Pump' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '170'
WHERE CoverageId = 'Basic' AND OptionId = 'Well Pump' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Well Pump', 'UNDER5000', 'Basic', 'CO', 170, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Well Pump' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '260'
WHERE CoverageId = 'Basic' AND OptionId = 'Well Pump' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Well Pump', 'OVER5000', 'Basic', 'CO', 260, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Well Pump' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '85'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Well Pump' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Well Pump', 'UNDER5000', 'Value Plus', 'CO', 85, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Well Pump' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '170'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Well Pump' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Well Pump', 'UNDER5000', 'Value Plus', 'CO', 170, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Well Pump' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '85'
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Well Pump' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Well Pump', 'UNDER5000', 'Eagle Premier', 'CO', 85, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Well Pump' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '170'
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Well Pump' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Well Pump', 'UNDER5000', 'Eagle Premier', 'CO', 170, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'HVAC Tune-Up' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '35'
WHERE CoverageId = 'Basic' AND OptionId = 'HVAC Tune-Up' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('HVAC Tune-Up', 'UNDER5000', 'Basic', 'CO', 35, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'HVAC Tune-Up' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '55'
WHERE CoverageId = 'Basic' AND OptionId = 'HVAC Tune-Up' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('HVAC Tune-Up', 'OVER5000', 'Basic', 'CO', 55, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'HVAC Tune-Up' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '70'
WHERE CoverageId = 'Basic' AND OptionId = 'HVAC Tune-Up' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('HVAC Tune-Up', 'UNDER5000', 'Basic', 'CO', 70, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'HVAC Tune-Up' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '110'
WHERE CoverageId = 'Basic' AND OptionId = 'HVAC Tune-Up' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('HVAC Tune-Up', 'OVER5000', 'Basic', 'CO', 110, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'HVAC Tune-Up' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '35'
WHERE CoverageId = 'Basic' AND OptionId = 'HVAC Tune-Up' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('HVAC Tune-Up', 'MULTIPLEX', 'Basic', 'CO', 35, 1,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'HVAC Tune-Up' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '70'
WHERE CoverageId = 'Basic' AND OptionId = 'HVAC Tune-Up' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('HVAC Tune-Up', 'MULTIPLEX', 'Basic', 'CO', 70, 1,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'HVAC Tune-Up' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '35'
WHERE CoverageId = 'Value Plus' AND OptionId = 'HVAC Tune-Up' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('HVAC Tune-Up', 'UNDER5000', 'Value Plus', 'CO', 35, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'HVAC Tune-Up' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '70'
WHERE CoverageId = 'Value Plus' AND OptionId = 'HVAC Tune-Up' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('HVAC Tune-Up', 'UNDER5000', 'Value Plus', 'CO', 70, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'HVAC Tune-Up' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '35'
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'HVAC Tune-Up' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('HVAC Tune-Up', 'UNDER5000', 'Eagle Premier', 'CO', 35, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'HVAC Tune-Up' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'CO'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '70'
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'HVAC Tune-Up' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'CO' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('HVAC Tune-Up', 'UNDER5000', 'Eagle Premier', 'CO', 70, 0,0, '', '', 2)

            END
            GO
            

            
--END of prop script for CO
