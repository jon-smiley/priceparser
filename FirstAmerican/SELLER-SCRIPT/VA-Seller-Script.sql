--Script for Option types stateId:VA



            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers Basic' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers Basic' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Basic', 'UNDER5000', 'Basic', 'VA', 0, 0,1, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers Basic' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers Basic' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'VA' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Basic', 'OVER5000', 'Basic', 'VA', 0, 0,1, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers Basic' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers Basic' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Basic', 'UNDER5000', 'Basic', 'VA', 0, 0,1, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers Basic' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers Basic' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'VA' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Basic', 'OVER5000', 'Basic', 'VA', 0, 0,1, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers Basic' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'VA'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers Basic' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'VA' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Basic', 'MULTIPLEX', 'Basic', 'VA', 0, 1,1, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers Basic' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'VA'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers Basic' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'VA' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Basic', 'MULTIPLEX', 'Basic', 'VA', 0, 1,1, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Sellers Basic' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Value Plus' AND OptionId = 'Sellers Basic' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Basic', 'UNDER5000', 'Value Plus', 'VA', 0, 0,1, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Sellers Basic' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Value Plus' AND OptionId = 'Sellers Basic' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Basic', 'UNDER5000', 'Value Plus', 'VA', 0, 0,1, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Sellers Basic' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Sellers Basic' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Basic', 'UNDER5000', 'Eagle Premier', 'VA', 0, 0,1, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Sellers Basic' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Sellers Basic' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Basic', 'UNDER5000', 'Eagle Premier', 'VA', 0, 0,1, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers Heating/AC/Duct Package' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '70'
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers Heating/AC/Duct Package' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Heating/AC/Duct Package', 'UNDER5000', 'Basic', 'VA', 70, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers Heating/AC/Duct Package' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '105'
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers Heating/AC/Duct Package' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'VA' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Heating/AC/Duct Package', 'OVER5000', 'Basic', 'VA', 105, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers Heating/AC/Duct Package' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '70'
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers Heating/AC/Duct Package' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Heating/AC/Duct Package', 'UNDER5000', 'Basic', 'VA', 70, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers Heating/AC/Duct Package' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '105'
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers Heating/AC/Duct Package' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'VA' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Heating/AC/Duct Package', 'OVER5000', 'Basic', 'VA', 105, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers Heating/AC/Duct Package' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'VA'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '70'
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers Heating/AC/Duct Package' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'VA' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Heating/AC/Duct Package', 'MULTIPLEX', 'Basic', 'VA', 70, 1,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers Heating/AC/Duct Package' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'VA'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '70'
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers Heating/AC/Duct Package' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'VA' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Heating/AC/Duct Package', 'MULTIPLEX', 'Basic', 'VA', 70, 1,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Sellers Heating/AC/Duct Package' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '70'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Sellers Heating/AC/Duct Package' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Heating/AC/Duct Package', 'UNDER5000', 'Value Plus', 'VA', 70, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Sellers Heating/AC/Duct Package' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '70'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Sellers Heating/AC/Duct Package' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Heating/AC/Duct Package', 'UNDER5000', 'Value Plus', 'VA', 70, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Sellers Heating/AC/Duct Package' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '70'
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Sellers Heating/AC/Duct Package' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Heating/AC/Duct Package', 'UNDER5000', 'Eagle Premier', 'VA', 70, 0,0, '', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Sellers Heating/AC/Duct Package' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '70'
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Sellers Heating/AC/Duct Package' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers Heating/AC/Duct Package', 'UNDER5000', 'Eagle Premier', 'VA', 70, 0,0, '', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers First Class Upgrade' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1, IncludedConditions = 'First Class Upgrade'
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers First Class Upgrade' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers First Class Upgrade', 'UNDER5000', 'Basic', 'VA', 0, 0,1, 'First Class Upgrade', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers First Class Upgrade' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1, IncludedConditions = 'First Class Upgrade'
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers First Class Upgrade' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'VA' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers First Class Upgrade', 'OVER5000', 'Basic', 'VA', 0, 0,1, 'First Class Upgrade', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers First Class Upgrade' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1, IncludedConditions = 'First Class Upgrade'
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers First Class Upgrade' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers First Class Upgrade', 'UNDER5000', 'Basic', 'VA', 0, 0,1, 'First Class Upgrade', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers First Class Upgrade' 
                    AND PropertyTypeCategory = 'OVER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1, IncludedConditions = 'First Class Upgrade'
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers First Class Upgrade' AND PropertyTypeCategory = 'OVER5000' AND StateId = 'VA' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers First Class Upgrade', 'OVER5000', 'Basic', 'VA', 0, 0,1, 'First Class Upgrade', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers First Class Upgrade' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'VA'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1, IncludedConditions = 'First Class Upgrade'
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers First Class Upgrade' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'VA' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers First Class Upgrade', 'MULTIPLEX', 'Basic', 'VA', 0, 1,1, 'First Class Upgrade', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Basic' 
                    AND OptionId = 'Sellers First Class Upgrade' 
                    AND PropertyTypeCategory = 'MULTIPLEX'
                    AND StateId = 'VA'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1, IncludedConditions = 'First Class Upgrade'
WHERE CoverageId = 'Basic' AND OptionId = 'Sellers First Class Upgrade' AND PropertyTypeCategory = 'MULTIPLEX' AND StateId = 'VA' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers First Class Upgrade', 'MULTIPLEX', 'Basic', 'VA', 0, 1,1, 'First Class Upgrade', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Sellers First Class Upgrade' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1, IncludedConditions = 'First Class Upgrade'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Sellers First Class Upgrade' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers First Class Upgrade', 'UNDER5000', 'Value Plus', 'VA', 0, 0,1, 'First Class Upgrade', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Value Plus' 
                    AND OptionId = 'Sellers First Class Upgrade' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1, IncludedConditions = 'First Class Upgrade'
WHERE CoverageId = 'Value Plus' AND OptionId = 'Sellers First Class Upgrade' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers First Class Upgrade', 'UNDER5000', 'Value Plus', 'VA', 0, 0,1, 'First Class Upgrade', '', 2)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Sellers First Class Upgrade' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '1'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1, IncludedConditions = 'First Class Upgrade'
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Sellers First Class Upgrade' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 1

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers First Class Upgrade', 'UNDER5000', 'Eagle Premier', 'VA', 0, 0,1, 'First Class Upgrade', '', 1)

            END
            GO
            

            

            IF EXISTS (
                SELECT * FROM [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices] 
                WHERE CoverageId = 'Eagle Premier' 
                    AND OptionId = 'Sellers First Class Upgrade' 
                    AND PropertyTypeCategory = 'UNDER5000'
                    AND StateId = 'VA'
                    AND YearCoverage = '2'
            )
            BEGIN
                UPDATE [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
SET Fee = '0', IsIncluded = 1, IncludedConditions = 'First Class Upgrade'
WHERE CoverageId = 'Eagle Premier' AND OptionId = 'Sellers First Class Upgrade' AND PropertyTypeCategory = 'UNDER5000' AND StateId = 'VA' AND YearCoverage = 2

            END
            ELSE
            BEGIN
                INSERT INTO [TransactionDesk].[dbo].[tblFirstAmericanOptionChoices]
([OptionId],[PropertyTypeCategory],[CoverageId],[StateId],[Fee],[IsFeeByUnit],[IsIncluded],[IncludedConditions],[Comments],[YearCoverage])
VALUES ('Sellers First Class Upgrade', 'UNDER5000', 'Eagle Premier', 'VA', 0, 0,1, 'First Class Upgrade', '', 2)

            END
            GO
            

            
--END of prop script for VA
