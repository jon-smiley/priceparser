
PRINT N'Creating [dbo].[tblFirstAmericanCoverageChoices] table'

CREATE TABLE [dbo].[tblFirstAmericanCoverageChoices] (
   [UIDCoverageChoice] [int] IDENTITY (1, 1) NOT NULL,
   [CoverageId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [PropertyTypeId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [StateId] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [Fee] [int] NOT NULL
) 

ALTER TABLE [dbo].[tblFirstAmericanCoverageChoices] ADD CONSTRAINT [PK_tblFirstAmericanCoverageChoices] PRIMARY KEY CLUSTERED ([UIDCoverageChoice])


PRINT N'Creating [dbo].[tblFirstAmericanCoverages] table'

CREATE TABLE [dbo].[tblFirstAmericanCoverages] (
   [UIDCoverage] [int] IDENTITY (1, 1) NOT NULL,
   [CoverageId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [CoverageName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [CoverageNameSpanish] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [CoverageNameFrench] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [DisplayOrder] [int] NOT NULL
) 

ALTER TABLE [dbo].[tblFirstAmericanCoverages] ADD CONSTRAINT [PK_tblFirstAmericanCoverages] PRIMARY KEY CLUSTERED ([UIDCoverage])


PRINT N'Creating [dbo].[tblFirstAmericanOptionChoices] table'

CREATE TABLE [dbo].[tblFirstAmericanOptionChoices] (
   [UIDOptionChoice] [int] IDENTITY (1, 1) NOT NULL,
   [OptionId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [PropertyTypeCategory] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [CoverageId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [StateId] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [Fee] [int] NOT NULL,
   [IsFeeByUnit] [bit] NOT NULL,
   [IsIncluded] [bit] NOT NULL,
   [IncludedConditions] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [Comments] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) 

ALTER TABLE [dbo].[tblFirstAmericanOptionChoices] ADD CONSTRAINT [PK_tblFirstAmericanOptionChoices] PRIMARY KEY CLUSTERED ([UIDOptionChoice])


PRINT N'Creating [dbo].[tblFirstAmericanOptions] table'

CREATE TABLE [dbo].[tblFirstAmericanOptions] (
   [UIDOption] [int] IDENTITY (1, 1) NOT NULL,
   [OptionId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [OptionName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [OptionCategory] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [DisplayOrder] [int] NOT NULL,
   [Comments] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) 

ALTER TABLE [dbo].[tblFirstAmericanOptions] ADD CONSTRAINT [PK_tblFirstAmericanOptions] PRIMARY KEY CLUSTERED ([UIDOption])

PRINT N'Creating [dbo].[tblFirstAmericanOptionsTranslations] table'

CREATE TABLE [dbo].[tblFirstAmericanOptionsTranslations] (
   [UIDOptionTranslation] [int] IDENTITY (1, 1) NOT NULL,
   [OptionId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [OptionNameSpanish] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [OptionNameFrench] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
) 

ALTER TABLE [dbo].[tblFirstAmericanOptionsTranslations] ADD CONSTRAINT [PK_tblFirstAmericanOptionsTranslations] PRIMARY KEY CLUSTERED ([UIDOptionTranslation])


PRINT N'Creating [dbo].[tblFirstAmericanPropertyTypes] table'

CREATE TABLE [dbo].[tblFirstAmericanPropertyTypes] (
   [UIDPropertyType] [int] IDENTITY (1, 1) NOT NULL,
   [PropertyTypeId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
   [PropertyTypeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [PropertyTypeCategory] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [OptionsMultiplier] [int] NOT NULL,
   [DisplayOrder] [int] NOT NULL,
   [StateIds] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) 

ALTER TABLE [dbo].[tblFirstAmericanPropertyTypes] ADD CONSTRAINT [PK_tblFirstAmericanPropertyTypes] PRIMARY KEY CLUSTERED ([UIDPropertyType])


PRINT N'Creating [dbo].[tblFirstAmericanRepresentatives] table'

CREATE TABLE [dbo].[tblFirstAmericanRepresentatives] (
   [UIDRepresentative] [int] IDENTITY (1, 1) NOT NULL,
   [RepresentativeName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [StateId] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) 

ALTER TABLE [dbo].[tblFirstAmericanRepresentatives] ADD CONSTRAINT [PK_tblFirstAmericanRepresentatives] PRIMARY KEY CLUSTERED ([UIDRepresentative])


PRINT N'Creating [dbo].[tblFirstAmericanServiceFees] table'

CREATE TABLE [dbo].[tblFirstAmericanServiceFees] (
   [UIDServiceFee] [int] IDENTITY (1, 1) NOT NULL,
   [CoverageId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [StateId] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [Fee] [int] NOT NULL
) 

ALTER TABLE [dbo].[tblFirstAmericanServiceFees] ADD CONSTRAINT [PK_tblFirstAmericanServiceFees] PRIMARY KEY CLUSTERED ([UIDServiceFee])


PRINT N'Creating [dbo].[tblFirstAmericanStates] table'

CREATE TABLE [dbo].[tblFirstAmericanStates] (
   [UIDState] [int] IDENTITY (1, 1) NOT NULL,
   [StateId] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [StateName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
   [Website] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
   [Brochure] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) 

ALTER TABLE [dbo].[tblFirstAmericanStates] ADD CONSTRAINT [PK_tblFirstAmericanStates] PRIMARY KEY CLUSTERED ([UIDState])
